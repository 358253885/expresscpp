### 0.1.0 (2019-10-20)

##### Chores

*  remove qtcreator build stuff ([9a94ee7e](git@gitlab.com:expresscpp/expresscpp/commit/9a94ee7e8b6f3550faadcbae8225b68746f800ac))
*  basic structure ([b84c008b](git@gitlab.com:expresscpp/expresscpp/commit/b84c008b6e2425131413bff5f30fb2802a788fa6))

##### Continuous Integration

*  add change tools ([79559eb9](git@gitlab.com:expresscpp/expresscpp/commit/79559eb9fae0b4d92b89ae5edcbc759b75053817))
*  add first tests ([726e8b2d](git@gitlab.com:expresscpp/expresscpp/commit/726e8b2d98d871cd29e9f19ae087e70b0c4e7d21))
*  add basic linux build job ([3036845d](git@gitlab.com:expresscpp/expresscpp/commit/3036845d8555801031f169b8b7a3d226b4c7cb8a))

##### Documentation Changes

*  add expressjs pendents to expresscpp examples ([e5ab0465](git@gitlab.com:expresscpp/expresscpp/commit/e5ab0465663f788d615da05ad872197efa87a93c))
*  add license ([82afc0b3](git@gitlab.com:expresscpp/expresscpp/commit/82afc0b35a1a35c48e1aa212daea97cf57cad3ed))
*  add logo to readme ([6a29c910](git@gitlab.com:expresscpp/expresscpp/commit/6a29c91091c1e36612564caa81a3c73eec88a064))
*  add link to examples to readme [ci skip] ([528a3734](git@gitlab.com:expresscpp/expresscpp/commit/528a37345cb35342a7155dafdb3d055f8e477a3d))
*  add example for basic static file server [ci skip] ([71259b09](git@gitlab.com:expresscpp/expresscpp/commit/71259b09112c22a2a232129cc12357ee24c071b9))
*  add build and coverage badge ([3b7c9d30](git@gitlab.com:expresscpp/expresscpp/commit/3b7c9d309a6409a73b5c6bfc5157495f580992ca))
*  add design decisions ([5c3ad8c5](git@gitlab.com:expresscpp/expresscpp/commit/5c3ad8c593f503d4b52c7a713139f1403251e3ec))

##### New Features

*   package library as conan library ([e6bca6a6](git@gitlab.com:expresscpp/expresscpp/commit/e6bca6a6c24dbb4d4620c993b40bb33a602dad20))
*  add stack print out for routing ([af3c79f0](git@gitlab.com:expresscpp/expresscpp/commit/af3c79f0f20d8d95a81db6d7318bbb617ee713ec))
*  create initial static file middleware ([3969a028](git@gitlab.com:expresscpp/expresscpp/commit/3969a028381d251bc5c7bd334736431d0358351e))
*  implement basic http methods ([f912a382](git@gitlab.com:expresscpp/expresscpp/commit/f912a382fd846001b10858c940141da20c56cbff))
*  implement basic router - handler map ([271ab022](git@gitlab.com:expresscpp/expresscpp/commit/271ab02203de9862cace7fd0390189b2093b9136))
*  add path and method to main handler ([75e60fe5](git@gitlab.com:expresscpp/expresscpp/commit/75e60fe5fa59beffb473baaa7133a9e8532b6ec3))
*  add first working webserver ([92899954](git@gitlab.com:expresscpp/expresscpp/commit/928999543efd69d9751e3df21d0bd12c7917b664))

